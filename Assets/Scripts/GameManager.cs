﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int collectables;
    int collected;
    public Text scoreText;
    public bool paused;
    public bool win;
    public GameObject pauseMenu;
    //Are power ups collected
    public bool waterUp;
    public bool fireUp;
    public bool windUp;
    public bool earthUp;
    public bool voidUp;

    // Start is called before the first frame update
    void Start()
    {
        collected = 0;
        waterUp = false;
        fireUp = false;
        windUp = false;
        earthUp = false;
        voidUp = false;
        paused = false;
        win = false;
        scoreText.text = "Crystals Collected \n 0 /" + collectables;
    }

    private void Update()
    {
        scoreText.text = "Crystals Collected \n" + collected + "/" + collectables;
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            paused = !paused;
        }
        pauseMenu.SetActive(paused);
    }

    public void PowerUp(int x)
    {
        switch(x)
        {
            case 0:
                {
                    windUp = true;
                    break;
                }
            case 1:
                {
                    waterUp = true;
                    break;
                }
            case 2:
                {
                    earthUp = true;
                    break;
                }
            case 3:
                {
                    fireUp = true;
                    break;
                }
            case 4:
                {
                    voidUp = true;
                    break;
                }
        }
    }

    public bool allCollected()
    {
        if (collected / collectables == 1)
            return true;
        else
            return false;
    }

    public void addCoin()
    {
        collected++;
    }

    public void Unpause()
    {
        paused = false;
    }
}
