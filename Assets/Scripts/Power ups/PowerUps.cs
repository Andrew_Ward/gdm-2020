﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    public enum totem {water,fire,earth,wind,black};
    public totem powerUp;
    public GameObject gmObject;
    GameManager gm;


    // Start is called before the first frame update
    void Start()
    {
        gm = gmObject.GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        switch(powerUp)
        {
            case totem.wind:
                {
                    gm.PowerUp(0);
                    break;
                }
            case totem.water:
                {
                    gm.PowerUp(1);
                    break;
                }
            case totem.earth:
                {
                    gm.PowerUp(2);
                    break;
                }
            case totem.fire:
                {
                    gm.PowerUp(3);
                    break;
                }
            case totem.black:
                {
                    gm.PowerUp(4);
                    break;
                }
        }
    }
}
