﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePower : MonoBehaviour
{
    public GameObject gm;
    public GameObject bush;
    bool inside = false;

    // Update is called once per frame
    void Update()
    {
        if(inside && gm.GetComponent<GameManager>().fireUp && Input.GetKeyDown(KeyCode.E))
        {
            Destroy(bush);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        inside = true;
    }

    private void OnTriggerExit(Collider other)
    {
        inside = false;
    }
}
