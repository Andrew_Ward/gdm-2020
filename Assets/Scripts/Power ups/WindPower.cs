﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPower : MonoBehaviour
{
    public GameObject gm;
    public GameObject fog;
    bool inside = false;

    // Update is called once per frame
    void Update()
    {
        if (inside && gm.GetComponent<GameManager>().windUp && Input.GetKeyDown(KeyCode.E))
        {
            Destroy(fog);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        inside = true;
    }

    private void OnTriggerExit(Collider other)
    {
        inside = false;
    }
}
