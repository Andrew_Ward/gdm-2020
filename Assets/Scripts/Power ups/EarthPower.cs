﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthPower : MonoBehaviour
{
    public GameObject gm;
    public GameObject Hill;
    bool inside = false;

    // Update is called once per frame
    void Update()
    {
        if (inside && gm.GetComponent<GameManager>().earthUp && Input.GetKeyDown(KeyCode.E))
        {
            Hill.SetActive(false);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        inside = true;
    }

    private void OnTriggerExit(Collider other)
    {
        inside = false;
    }
}
