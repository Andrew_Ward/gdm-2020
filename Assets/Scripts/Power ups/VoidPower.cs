﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VoidPower : MonoBehaviour
{
    public GameObject gm;
    public Text winText;
    public float winDelay;
    bool inside = false;


    // Update is called once per frame
    void Update()
    {
        if (inside && gm.GetComponent<GameManager>().voidUp && Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(setWinText());
            
        }
        if (Input.anyKey && gm.GetComponent<GameManager>().win)
        {
            Cursor.lockState = CursorLockMode.None;
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.buildIndex - 1);
        }
    }

    IEnumerator setWinText()
    {
        winText.gameObject.SetActive(true);
        if(gm.GetComponent<GameManager>().allCollected())
        {
            winText.text = "I remember it all ...";
        }
        else
        {
            winText.text = "I feel like something is missing";
        }
        
        yield return new WaitForSeconds(winDelay);
        gm.GetComponent<GameManager>().win = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        inside = true;
    }

    private void OnTriggerExit(Collider other)
    {
        inside = false;
    }
}
