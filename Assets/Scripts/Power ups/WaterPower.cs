﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPower : MonoBehaviour
{
    public GameObject gm;
    public GameObject water;
    bool inside = false;

    // Update is called once per frame
    void Update()
    {
        if (inside && gm.GetComponent<GameManager>().waterUp && Input.GetKeyDown(KeyCode.E))
        {
            water.SetActive(false);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        inside = true;
    }

    private void OnTriggerExit(Collider other)
    {
        inside = false;
    }
}
