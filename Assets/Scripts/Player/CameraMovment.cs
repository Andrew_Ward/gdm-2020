﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovment : MonoBehaviour
{
    public float senesitivity = 100;

    public Transform player;

    public GameObject gm;

    float xRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if(gm.GetComponent<GameManager>().paused)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else if(!gm.GetComponent<GameManager>().win)
        {
            Cursor.lockState = CursorLockMode.Locked;
            float mouseX = Input.GetAxis("Mouse X") * senesitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * senesitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -50f, 50f);

            transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

            player.Rotate(Vector3.up * mouseX);
        }
    }
}
