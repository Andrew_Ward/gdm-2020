﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour
{
    public bool coin;
    public GameObject gmObject;
    GameManager gm;

    // Start is called before the first frame update
    private void Start()
    {
        gm = gmObject.GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(coin)
        {
            gm.addCoin();
        }
        Destroy(this.gameObject);
    }
}
